import json
import pika
import datetime
import time
## importing 'mysql.connector' as mysql for convenient
import mysql.connector as mysql

class SamplePublish():

    RABBITMQ_IP  = "rabbitmq_ip"
    RABBITMQ_PORT = "rabbitmq_port"
    TIME_LAG = "time_lag"
    MYSQL_IP = "mysql_host"
    MYSQL_PORT = "mysql_port"
    MYSQL_USER = "mysql_user"
    MYSQL_PWD = "mysql_password"
    MYSQL_DB = "mysql_database"

    def __init__(self):

        with open("../config/dev.json") as f:
            self.config = json.load(f)

        # Create a new channel with the next available channel number or pass
        # in a channel number to use
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(self.config[self.RABBITMQ_IP],
                                      self.config[self.RABBITMQ_PORT]))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue='xpresspo')


    def publish(self):

        print("Press Ctrl + C to stop sending messages")
        while True:
            message = "Message : " + str(datetime.datetime.now())
            self.channel.basic_publish(exchange='', routing_key='xpresspo',
                                  body=message)
            print("[X] Published Message : {}".format(message))
            time.sleep(self.config[self.TIME_LAG])

if __name__=="__main__":
    job  = SamplePublish()
    job.publish()
